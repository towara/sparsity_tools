#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Sparse>
#include <iostream>

#include <unordered_set>
//#include "../sfl-library/include/sfl/small_flat_set.hpp"
#include "../jsp/jsp.hpp"

const int N = 10;

// f is designed, such that the Jacobian sparsity pattern of f is the same as A
template<typename MAT, typename T>
Eigen::VectorX<T> f(MAT& A, Eigen::VectorX<T>& x){
    return A*x;
}

int main(){
    // choose which implementation of set to use, std::set might be slow
    using my_jsp = jsp<std::set<int>>;
    //using my_jsp = jsp<std::unordered_set<int>>;
    //using my_jsp = jsp<sfl::small_flat_set<int,4>>;

    // create a Matrix B with approx 10% non-zeros
    Eigen::MatrixX<my_jsp> A = Eigen::MatrixX<my_jsp>::Random(N,5*N);
    A = (A.array() > 0.9).cast<my_jsp>();
    Eigen::SparseMatrix<my_jsp> B = A.sparseView();

    Eigen::VectorX<my_jsp> x = Eigen::VectorX<my_jsp>::Ones(A.cols());
    for(int i=0; i<x.size(); i++){
        x[i].nz.insert(i);
    }
    Eigen::VectorX<my_jsp> y = f(B,x);

    // y[i] will depend on all non-zeros in the i-th row of B
    for(int i=0; i<N; i++){
        std::cout << "y[" << i << "] = ";
        for(auto nz : y[i].nz){
            std::cout << nz << " ";
        } std::cout << std::endl;
    }

    // print sparsity pattern of Jacobian
    std::cout << "\nSparsity pattern of df/dx:" << std::endl;
    for(int i=0; i<y.size(); i++){
        for(int j=0; j<x.size(); j++){
          // if id j is in nz set of y[i] print "*", else print "."
          std::cout << (y[i].nz.count(j) ? "*"  : ".");
        }
        std::cout << std::endl;
    }
}
