#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Sparse>
#include <iostream>

#include <unordered_set>
//#include "../sfl-library/include/sfl/small_flat_set.hpp"
#include "../hsp/hsp.hpp"

using hsp_t = hsp<>;

const int N = 20;

template<typename BinaryOp>
  struct Eigen::ScalarBinaryOpTraits<hsp_t, double, BinaryOp>
{
    typedef hsp_t ReturnType;
};
template<typename BinaryOp>
struct Eigen::ScalarBinaryOpTraits<double, hsp_t, BinaryOp>
{
    typedef hsp_t ReturnType;
};


// f is designed, such that the Hessian sparsity pattern of f is the same as A
template <typename D, typename T>
T f(Eigen::SparseMatrix<D> &A, Eigen::VectorX<T> &x)
{
    T y = 0.0;

    for (int k = 0; k < A.outerSize(); ++k)
    {
        for (typename Eigen::SparseMatrix<D>::InnerIterator it(A, k); it; ++it)
        {
            if (it.row() >= it.col())
            {
                y += x[it.row()] * x[it.col()];
            }
        }
    }

    return y;
}

int main(){
    Eigen::MatrixX<double> A = Eigen::MatrixX<double>::Random(N,N);
    A = (A.array() > 0.8).cast<double>();
    // make A symmetric
    A = A + A.transpose().eval();
    Eigen::SparseMatrix<double> B = A.sparseView();

    Eigen::VectorX<hsp_t> x = Eigen::VectorX<hsp_t>::Ones(A.cols());
    for(int i=0; i<x.size(); i++){
        x[i].fod.insert(i);
    }
    hsp_t y = f(B,x);

    std::cout << "A: " << std::endl;
    std::cout << A << std::endl;


    std::cout << "Nonzero pattern of Gradient df/dx: ";
    for(int i=0; i<N; i++){
        std::cout << (y.fod.count(i) ? "*" : ".");
    }
    std::cout << std::endl;

    std::cout << "Nonzero pattern of Hessian d2f/dx2:" << std::endl;
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            std::cout << (y.sod.count({i,j}) + y.sod.count({j,i}) ? "*" : ".");
        } std::cout << std::endl;
    }
}
