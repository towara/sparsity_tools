#ifndef HSP_INCLUDED_
#define HSP_INCLUDED_

#include <set>
#include <iostream>

using std::pair;

template <typename fod_t = std::set<int>, typename sod_t = std::set<std::pair<int, int>>>
struct hsp
{
  double v;

  // using fod_t = set_t<int>;
  // using sod_t = set_t<pair<int,int>>;
  fod_t fod;
  sod_t sod;

  hsp(const double &x);
  hsp();
  hsp(const hsp<fod_t, sod_t> &x);
  hsp(double v, const fod_t& fod, const sod_t& sod);
  hsp(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
  hsp<fod_t, sod_t> &operator=(const hsp<fod_t, sod_t> &x);
  hsp<fod_t, sod_t> &operator=(const double &a);
  hsp<fod_t, sod_t> &operator+=(const hsp<fod_t, sod_t> &x);

  template <typename T, typename S>
  friend std::ostream &operator<<(std::ostream &os, const hsp<T,S> &x);
};

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator*(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator*(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator*(const hsp<fod_t, sod_t> &x1, const double &a2);

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator+(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator+(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator+(const hsp<fod_t, sod_t> &x1, const double &a2);

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator-(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator-(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator-(const hsp<fod_t, sod_t> &x1, const double &a2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator-(const hsp<fod_t, sod_t> &x);

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator/(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator/(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator/(const hsp<fod_t, sod_t> &x1, const double &a2);

template <typename fod_t, typename sod_t>
bool operator<(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator<(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator<(const hsp<fod_t, sod_t> &x1, const double &a2);

template <typename fod_t, typename sod_t>
bool operator<=(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator<=(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator<=(const hsp<fod_t, sod_t> &x1, const double &a2);

template <typename fod_t, typename sod_t>
bool operator>(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator>(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator>(const hsp<fod_t, sod_t> &x1, const double &a2);

template <typename fod_t, typename sod_t>
bool operator>=(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator>=(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator>=(const hsp<fod_t, sod_t> &x1, const double &a2);

template <typename fod_t, typename sod_t>
bool operator==(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator==(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator==(const hsp<fod_t, sod_t> &x1, const double &a2);

template <typename fod_t, typename sod_t>
bool operator!=(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator!=(const double &a1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
bool operator!=(const hsp<fod_t, sod_t> &x1, const double &a2);

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> sin(const hsp<fod_t, sod_t> &x);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> cos(const hsp<fod_t, sod_t> &x);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> exp(const hsp<fod_t, sod_t> &x);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> atan(const hsp<fod_t, sod_t> &x);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> sqrt(const hsp<fod_t, sod_t> &x);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> log(const hsp<fod_t, sod_t> &x);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> log10(const hsp<fod_t, sod_t> &x);

template <typename fod_t, typename sod_t>
int ceil(const hsp<fod_t, sod_t> &x);

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> pow(const hsp<fod_t, sod_t> &x, const double &c);
// template<typename fod_t, typename sod_t>
// hsp<fod_t, sod_t> pow(const hsp<fod_t, sod_t>& x, int n);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> pow(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2);
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> pow(const double &a1, const hsp<fod_t, sod_t> &x2);

#include "hsp_impl.hpp"

#endif
