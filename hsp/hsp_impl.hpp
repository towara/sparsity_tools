#include <cmath>
#include <iostream>
#include <algorithm>
//#include <functional>
#include <cassert>
#include "hsp.hpp"

// template<typename fod_t, typename sod_t>
// int hsp<fod_t, sod_t>::n=0;

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t>::hsp(const double &x) : v(x), fod(), sod(){};
template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t>::hsp() : v(0), fod(), sod(){};

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t>::hsp(const hsp<fod_t, sod_t> &x) : v(x.v), fod(x.fod), sod(x.sod){};

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t>::hsp(double v, const fod_t& fod, const sod_t& sod) : v(v), fod(fod), sod(sod) {}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t>::hsp(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2) : fod(x1.fod), sod(x1.sod)
{
  fod.insert(x2.fod.begin(), x2.fod.end());
  sod.insert(x2.sod.begin(), x2.sod.end());
};

template <typename T, typename S>
std::ostream &operator<<(std::ostream &os, const hsp<T,S> &x){
  os << x.v;
  return os;
}

template <typename fod_t, typename sod_t>
sod_t permute_fod(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  sod_t sod;
  for (const auto &f1 : x1.fod)
  {
    for (const auto &f2 : x2.fod){
      //if(f1 <= f2) /* use symmetry */
        int min_f = std::min(f1,f2);
        int max_f = std::max(f1,f2);
        sod.insert({min_f, max_f});
        //sod.insert({f1, f2});
    }
  }
  return sod;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> &hsp<fod_t, sod_t>::operator=(const hsp<fod_t, sod_t> &x)
{
  if (this == &x)
    return *this;
  v = x.v;
  fod = x.fod;
  sod = x.sod;
  return *this;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> &hsp<fod_t, sod_t>::operator=(const double &a)
{
  this->v = a;
  fod.clear();
  sod.clear();
  return *this;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> &hsp<fod_t, sod_t>::operator+=(const hsp<fod_t, sod_t> &x)
{
  *this = *this + x;
  return *this;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator*(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x1, x2);
  tmp.v = x1.v * x2.v;
  sod_t sod = permute_fod(x1, x2);
  tmp.sod.insert(sod.begin(), sod.end());
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator*(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x2);
  tmp.v = a1 * x2.v;
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator*(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  hsp<fod_t, sod_t> tmp(x1);
  tmp.v = x1.v * a2;
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator+(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x1, x2);
  tmp.v = x1.v + x2.v;
  // tmp.sod.insert(permute_fod(x1,x2)); // wrong!
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator+(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x2);
  tmp.v = a1 + x2.v;
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator+(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  hsp<fod_t, sod_t> tmp(x1);
  tmp.v = x1.v + a2;
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator-(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x1, x2);
  tmp.v = x1.v - x2.v;
  // tmp.sod.insert(permute_fod(x1,x2)); // wrong!
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator-(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x2);
  tmp.v = a1 - x2.v;
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator-(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  hsp<fod_t, sod_t> tmp(x1);
  tmp.v = x1.v - a2;
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator-(const hsp<fod_t, sod_t> &x1)
{
  hsp<fod_t, sod_t> tmp(x1);
  tmp.v = -x1.v;
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator/(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x1, x2);
  tmp.v = x1.v / x2.v;
  
  sod_t sod12 = permute_fod(x1, x2);
  sod_t sod22 = permute_fod(x2, x2);
  tmp.sod.insert(sod12.begin(), sod12.end());
  tmp.sod.insert(sod22.begin(), sod22.end());

  //tmp.sod.merge(permute_fod(x2, x2));
  //tmp.sod.merge(permute_fod(x1, x2));
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator/(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x2);
  tmp.v = a1 / x2.v;

  sod_t sod22 = permute_fod(x2, x2);
  tmp.sod.insert(sod22.begin(), sod22.end());

  //tmp.sod.merge(permute_fod(x2, x2));
  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> operator/(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  hsp<fod_t, sod_t> tmp(x1);
  tmp.v = x1.v / a2;
  return tmp;
}

template <typename fod_t, typename sod_t>
bool operator<(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  return (x1.v < x2.v);
}
template <typename fod_t, typename sod_t>
bool operator<(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  return (a1 < x2.v);
}
template <typename fod_t, typename sod_t>
bool operator<(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  return (x1.v < a2);
}

template <typename fod_t, typename sod_t>
bool operator<=(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  return (x1.v <= x2.v);
}
template <typename fod_t, typename sod_t>
bool operator<=(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  return (a1 <= x2.v);
}
template <typename fod_t, typename sod_t>
bool operator<=(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  return (x1.v <= a2);
}

template <typename fod_t, typename sod_t>
bool operator>(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  return (x1.v > x2.v);
}

template <typename fod_t, typename sod_t>
bool operator>(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  return (a1 > x2.v);
}

template <typename fod_t, typename sod_t>
bool operator>(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  return (x1.v > a2);
}

template <typename fod_t, typename sod_t>
bool operator>=(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  return (x1.v >= x2.v);
}

template <typename fod_t, typename sod_t>
bool operator>=(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  return (a1 >= x2.v);
}

template <typename fod_t, typename sod_t>
bool operator>=(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  return (x1.v >= a2);
}

template <typename fod_t, typename sod_t>
bool operator==(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  return (x1.v == x2.v);
}

template <typename fod_t, typename sod_t>
bool operator==(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  return (a1 == x2.v);
}

template <typename fod_t, typename sod_t>
bool operator==(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  return (x1.v == a2);
}

template <typename fod_t, typename sod_t>
bool operator!=(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  return (x1.v != x2.v);
}

template <typename fod_t, typename sod_t>
bool operator!=(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  return (a1 != x2.v);
}

template <typename fod_t, typename sod_t>
bool operator!=(const hsp<fod_t, sod_t> &x1, const double &a2)
{
  return (x1.v != a2);
}

#define HSP_UNARY_OP(OP)                                         \
  template <typename fod_t, typename sod_t>                      \
  hsp<fod_t, sod_t> OP(const hsp<fod_t, sod_t> &x)               \
  {                                                              \
    return hsp<fod_t, sod_t>(OP(x.v), x.fod, permute_fod(x, x)); \
  }                                                              \

HSP_UNARY_OP(sin)
HSP_UNARY_OP(cos)
HSP_UNARY_OP(exp)
HSP_UNARY_OP(atan)
HSP_UNARY_OP(sqrt)
HSP_UNARY_OP(log)
HSP_UNARY_OP(log10)
#undef HSP_UNARY_OP

template <typename fod_t, typename sod_t>
int ceil(const hsp<fod_t, sod_t> &x)
{
  return x.v;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> pow(const hsp<fod_t, sod_t> &x, const double &c)
{
  hsp<fod_t, sod_t> tmp(x);
  tmp.v = pow(x.v, c);
  
  sod_t sod = permute_fod(x, x);
  tmp.sod.insert(sod.begin(), sod.end());

  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> pow(const hsp<fod_t, sod_t> &x1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x1, x2);
  tmp.v = pow(x1.v, x2.v);
  
  sod_t sod = permute_fod(x1, x2);
  tmp.sod.insert(sod.begin(), sod.end());

  return tmp;
}

template <typename fod_t, typename sod_t>
hsp<fod_t, sod_t> pow(const double &a1, const hsp<fod_t, sod_t> &x2)
{
  hsp<fod_t, sod_t> tmp(x2);
  tmp.v = pow(a1, x2.v);
  
  sod_t sod = permute_fod(x2, x2);
  tmp.sod.insert(sod.begin(), sod.end());

  return tmp;
}
