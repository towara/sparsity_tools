#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <iostream>
#include <algorithm>
#include <stack>
#include <set>
#include <cassert>
#include<fstream>

#include "StarColoring.hpp"

struct row_t{
    int row;
    int nnzs;
};
struct col_t{
    int col;
    int nnzs;
};

StarColoring::StarColoring(const Eigen::SparseMatrix<double> &A)
:
A(A)
{
    assert(A.isApprox(A.transpose()));
}

void StarColoring::starColor()
{
    colors = Eigen::ArrayX<int>::Constant(A.cols(),-1);

    Eigen::ArrayX<col_t> nnz_col(A.cols());
    for(int i=0; i<A.cols(); i++){
        nnz_col[i] = {i,0};
    }
    // count nnzs per column
    for (int k = 0; k < A.outerSize(); ++k){
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(A, k); it; ++it){
            nnz_col[it.col()].nnzs++;
        }
    }

    // sort column vertices by number of edges, natural ordering as tie-breaker
    std::sort(nnz_col.begin(), nnz_col.end(), 
        [](col_t &a, col_t &b){return a.nnzs != b.nnzs ? a.nnzs >= b.nnzs : a.col <= b.col;});
    
    // distane two coloring
    for(int i=0; i<A.cols(); i++){
        int v = nnz_col[i].col;
        std::set<int> forbidden_colors;
        // distance one
        for (typename Eigen::SparseMatrix<double>::InnerIterator it1(A, v); it1; ++it1){
            int d1 = it1.row();
            if(colors[d1]>=0){
                forbidden_colors.insert(colors[d1]);
            }
            // distance two
            for (typename Eigen::SparseMatrix<double>::InnerIterator it2(A, d1); it2; ++it2){
                int d2 = it2.row();
                if(colors[d2]>=0){
                    if(colors[d1]==-1){ // distance one is not yet colored
                        forbidden_colors.insert(colors[d2]);
                    } else if (colors[d2] < colors[d1]){
                        forbidden_colors.insert(colors[d2]);
                    }
                }
            }
        }
        for(int color=0; color<A.cols();color++){
            if(forbidden_colors.count(color) == 0){
                colors[v] = color;
                break;
            }
        }
    }
    std::cout << "nColors: " << colors.maxCoeff()+1 << std::endl;
}

void StarColoring::distanceTwoColor()
{
    colors = Eigen::ArrayX<int>::Constant(A.cols(),-1);

    Eigen::ArrayX<col_t> nnz_col(A.cols());
    for(int i=0; i<A.cols(); i++){
        nnz_col[i] = {i,0};
    }
    // count nnzs per column
    for (int k = 0; k < A.outerSize(); ++k){
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(A, k); it; ++it){
            nnz_col[it.col()].nnzs++;
        }
    }

    // sort column vertices by number of edges, natural ordering as tie-breaker
    std::sort(nnz_col.begin(), nnz_col.end(), 
        [](col_t &a, col_t &b){return a.nnzs != b.nnzs ? a.nnzs >= b.nnzs : a.col <= b.col;});
    
    // distane two coloring
    for(int i=0; i<A.cols(); i++){
        int v = nnz_col[i].col;
        std::set<int> forbidden_colors;
        // distance one
        for (typename Eigen::SparseMatrix<double>::InnerIterator it1(A, v); it1; ++it1){
            int d1 = it1.row();
            if(colors[d1]>=0){
                forbidden_colors.insert(colors[d1]);
            }
            // distance two
            for (typename Eigen::SparseMatrix<double>::InnerIterator it2(A, d1); it2; ++it2){
                int d2 = it2.row();
                if(colors[d2]>=0){
                    forbidden_colors.insert(colors[d2]);
                }
            }
        }
        for(int color=0; color<A.cols();color++){
            if(forbidden_colors.count(color) == 0){
                colors[v] = color;
                break;
            }
        }
    }
    std::cout << "nColors: " << colors.maxCoeff()+1 << std::endl;
}

Eigen::MatrixXd StarColoring::getColumnSeedMatrix(){
    assert(colors.count() > 0 && "Matrix is not colored yet.");
    Eigen::MatrixXd S = Eigen::MatrixXd::Zero(colors.maxCoeff()+1, A.cols());
    
    for(int i=0; i<A.cols(); i++){
        S(colors[i],i) = 1.0;
    }
    
    return S;
}

Eigen::SparseMatrix<double> StarColoring::reconstructJacobian(Eigen::MatrixXd Jc){
    Eigen::SparseMatrix<double> J(A);
    for (int k = 0; k < J.outerSize(); ++k){
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(J, k); it; ++it)
            it.valueRef() = Jc(colors[it.col()], it.col());
    }
    return J;
}


void StarColoring::writeGraph(){
    /*std::cout << "digraph D {" << std::endl;
    for (int k = 0; k < A.outerSize(); ++k){
        std::cout << "  " << k << " [label=\"" << k << ": " << nnz_row[k].nnzs << "\"]" << std::endl;
    }
    for (int k = 0; k < A.outerSize(); ++k)
    {
        std::cout << "  " << k << " -> {";
        for (typename SparseMatrix<T>::InnerIterator it(A, k); it; ++it)
        {
            std::cout << it.col() << (it+1l ? ", " : "");
        }
        std::cout << "}" << std::endl;
    }
    std::cout << "}" << std::endl;*/

    // bipartite graph
    std::ofstream ofs("columns.dot");
    ofs << "graph G {" << std::endl;
    ofs << "  subgraph cluster_left {" << std::endl;
    for(int i=0; i<A.rows(); i++)
        ofs << "    r" << i << std::endl;
    ofs << "}" << std::endl;
    ofs << "subgraph cluster_right {" << std::endl;
    for(int i=0; i<A.cols(); i++)
        ofs << "    c" << i  << " [ fillcolor=" << colors[i]+1 << "  style=filled colorscheme=paired9]" << std::endl;
    ofs << "  }" << std::endl;
    for (int k = 0; k < A.outerSize(); ++k)
    {
        for (Eigen::SparseMatrix<double>::InnerIterator it(A, k); it; ++it)
        {
            ofs << "  r" << it.row() << " -- c" << it.col() << std::endl;
        }
    }
    ofs << "}" << std::endl;
}