#include <Eigen/Dense>
#include <Eigen/Sparse>


//template<typename T>
//using SparseMatrix = Eigen::SparseMatrix<T, Eigen::ColMajor>;

class BipartitePartialColoring{
    public:
        BipartitePartialColoring(const Eigen::SparseMatrix<double> &A);
        void colorColumns();
        void colorRows();
        Eigen::MatrixXd getColumnSeedMatrix();
        Eigen::MatrixXd getRowSeedMatrix();
        void writeGraph();
        Eigen::SparseMatrix<double> reconstructJacobian(Eigen::MatrixXd Jc);
        Eigen::SparseMatrix<double> reconstructJacobianRows(Eigen::MatrixXd Jc);
        double nColors();
        const Eigen::SparseMatrix<double>& sparseMatrix();
    private:
        Eigen::SparseMatrix<double> A;
        Eigen::ArrayX<int> colors;
        int nColors_;
};

/*std::ostream& operator<<(std::ostream& os, const row_t& r)
{
    os << "(" << r.row << "," << r.nnzs << ")";
    return os;
}
std::ostream& operator<<(std::ostream& os, const col_t& r)
{
    os << "(" << r.col << "," << r.nnzs << ")";
    return os;
}*/
