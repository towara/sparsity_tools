#include <Eigen/Dense>
#include <Eigen/Sparse>


//template<typename T>
//using SparseMatrix = Eigen::SparseMatrix<T, Eigen::ColMajor>;

class StarColoring{
    public:
        StarColoring(const Eigen::SparseMatrix<double> &A);
        void starColor();
        void distanceTwoColor();
        Eigen::MatrixXd getColumnSeedMatrix();
        void writeGraph();
        Eigen::SparseMatrix<double> reconstructJacobian(Eigen::MatrixXd Jc);
    private:
        Eigen::SparseMatrix<double> A;
        Eigen::ArrayX<int> colors;
};

/*std::ostream& operator<<(std::ostream& os, const row_t& r)
{
    os << "(" << r.row << "," << r.nnzs << ")";
    return os;
}
std::ostream& operator<<(std::ostream& os, const col_t& r)
{
    os << "(" << r.col << "," << r.nnzs << ")";
    return os;
}*/