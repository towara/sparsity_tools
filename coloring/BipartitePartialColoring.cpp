#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <iostream>
#include <algorithm>
#include <stack>
#include <set>
#include <cassert>
#include<fstream>

#include "BipartitePartialColoring.hpp"

struct row_t{
    int row;
    int nnzs;
};
struct col_t{
    int col;
    int nnzs;
};

BipartitePartialColoring::BipartitePartialColoring(const Eigen::SparseMatrix<double> &A)
:
A(A)
{

}

void BipartitePartialColoring::colorColumns()
{
    colors = Eigen::ArrayX<int>::Constant(A.cols(),-1);

    // A allows efficient traversal of columns, A^T of rows (ColMajor Storage)
    Eigen::SparseMatrix<double> At = A.transpose();

    Eigen::ArrayX<col_t> nnz_col(A.cols());
    for(int i=0; i<A.cols(); i++){
        nnz_col[i] = {i,0};
    }
    // count nnzs per column
    for (int k = 0; k < A.outerSize(); ++k){
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(A, k); it; ++it)
            nnz_col[it.col()].nnzs++;
    }

    // sort column vertices by number of edges, natural ordering as tie-breaker
    std::sort(nnz_col.begin(), nnz_col.end(), 
        [](col_t &a, col_t &b){return a.nnzs != b.nnzs ? a.nnzs >= b.nnzs : a.col <= b.col;});
    
    // partial distane two coloring
    for(int i=0; i<A.cols(); i++){
        int ci = nnz_col[i].col;
        std::set<int> forbidden_colors;
        // distance one into rows
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(A, ci); it; ++it){
            int ri = it.row();
            // distance two back into columns (rows in A^T)
            for (typename Eigen::SparseMatrix<double>::InnerIterator it2(At, ri); it2; ++it2){
                int cj = it2.row();
                if(colors[cj]>=0){
                    forbidden_colors.insert(colors[cj]);
                }
            }
        }
        for(int color=0; color<A.cols();color++){
            if(forbidden_colors.count(color) == 0){
                colors[ci] = color;
                break;
            }
        }
    }
    nColors_ = colors.maxCoeff()+1;
    std::cout << "nColColors: " << nColors_ << std::endl;
}

void BipartitePartialColoring::colorRows()
{
    colors = Eigen::ArrayX<int>::Constant(A.rows(),-1);

    // A allows efficient traversal of columns, A^T of rows (ColMajor Storage)
    Eigen::SparseMatrix<double> At = A.transpose();

    Eigen::ArrayX<row_t> nnz_row(A.rows());
    for(int i=0; i<A.rows(); i++){
        nnz_row[i] = {i,0};
    }
    // count nnzs per column
    for (int k = 0; k < A.outerSize(); ++k){
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(A, k); it; ++it)
            nnz_row[it.row()].nnzs++;
    }

    // sort column vertices by number of edges, natural ordering as tie-breaker
    std::sort(nnz_row.begin(), nnz_row.end(), 
        [](row_t &a, row_t &b){
            return a.nnzs != b.nnzs ? a.nnzs >= b.nnzs : a.row <= b.row;
    });
    
    // partial distane two coloring on rows of A = cols of A^T
    for(int i=0; i<A.rows(); i++){
        int ri = nnz_row[i].row;
        std::set<int> forbidden_colors;
        // distance one into cols (=rows in A^T)
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(At, ri); it; ++it){
            int ci = it.row();
            // distance two back into rows
            for (typename Eigen::SparseMatrix<double>::InnerIterator it2(A, ci); it2; ++it2){
                int rj = it2.row();
                if(colors[rj]>=0){
                    forbidden_colors.insert(colors[rj]);
                }
            }
        }
        for(int color=0; color<A.rows();color++){
            if(forbidden_colors.count(color) == 0){
                colors[ri] = color;
                break;
            }
        }
    }
    nColors_ = colors.maxCoeff()+1;
    std::cout << "nRowColors: " << nColors_ << std::endl;
}

Eigen::MatrixXd BipartitePartialColoring::getColumnSeedMatrix(){
    assert(colors.count() > 0 && "Matrix is not colored yet.");
    Eigen::MatrixXd S = Eigen::MatrixXd::Zero(nColors_, A.cols());
    
    for(int i=0; i<A.cols(); i++){
        S(colors[i],i) = 1.0;
    }
    
    return S;
}

Eigen::MatrixXd BipartitePartialColoring::getRowSeedMatrix(){
    assert(colors.count() > 0 && "Matrix is not colored yet.");
    Eigen::MatrixXd S = Eigen::MatrixXd::Zero(A.rows(), nColors_);
    
    for(int i=0; i<A.rows(); i++){
        S(i,colors[i]) = 1.0;
    }
    
    return S;
}

Eigen::SparseMatrix<double> BipartitePartialColoring::reconstructJacobian(Eigen::MatrixXd Jc){
    Eigen::SparseMatrix<double> J(A);
    for (int k = 0; k < J.outerSize(); ++k){
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(J, k); it; ++it)
            it.valueRef() = Jc(it.row(), colors[it.col()]);
    }
    return J;
}

Eigen::SparseMatrix<double> BipartitePartialColoring::reconstructJacobianRows(Eigen::MatrixXd Jc){
    Eigen::SparseMatrix<double> J(A);
    for (int k = 0; k < J.outerSize(); ++k){
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(J, k); it; ++it)
            it.valueRef() = Jc(colors[it.row()], it.col());
    }
    return J;
}

double BipartitePartialColoring::nColors(){
    return nColors_;
}

const Eigen::SparseMatrix<double>& BipartitePartialColoring::sparseMatrix(){
    return A;
}

void BipartitePartialColoring::writeGraph(){
    /*std::cout << "digraph D {" << std::endl;
    for (int k = 0; k < A.outerSize(); ++k){
        std::cout << "  " << k << " [label=\"" << k << ": " << nnz_row[k].nnzs << "\"]" << std::endl;
    }
    for (int k = 0; k < A.outerSize(); ++k)
    {
        std::cout << "  " << k << " -> {";
        for (typename SparseMatrix<T>::InnerIterator it(A, k); it; ++it)
        {
            std::cout << it.col() << (it+1l ? ", " : "");
        }
        std::cout << "}" << std::endl;
    }
    std::cout << "}" << std::endl;*/

    // bipartite graph
    std::ofstream ofs("columns.dot");
    ofs << "graph G {" << std::endl;
    ofs << "  subgraph cluster_left {" << std::endl;
    for(int i=0; i<A.rows(); i++)
        ofs << "    r" << i << std::endl;
    ofs << "}" << std::endl;
    ofs << "subgraph cluster_right {" << std::endl;
    for(int i=0; i<A.cols(); i++)
        ofs << "    c" << i  << " [ fillcolor=" << colors[i]+1 << "  style=filled colorscheme=paired9]" << std::endl;
    ofs << "  }" << std::endl;
    for (int k = 0; k < A.outerSize(); ++k)
    {
        for (Eigen::SparseMatrix<double>::InnerIterator it(A, k); it; ++it)
        {
            ofs << "  r" << it.row() << " -- c" << it.col() << std::endl;
        }
    }
    ofs << "}" << std::endl;
}
