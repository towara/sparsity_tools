#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <iostream>
#include <algorithm>
#include <stack>
#include <set>

#include"BipartitePartialColoring.hpp"

const int N = 50;

int main(){
    Eigen::MatrixX<double> A = Eigen::MatrixX<double>::Random(N,N);
    A = (A.array() > 0.9).cast<double>();
    Eigen::SparseMatrix<double> S = A.sparseView();
    
    BipartitePartialColoring G(S);
    G.colorColumns();

    std::cout << A << std::endl << std::endl;
    std::cout << G.getColumnSeedMatrix() << std::endl;

    //G.writeGraph();
}