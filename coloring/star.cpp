#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <iostream>
#include <algorithm>
#include <stack>
#include <set>

#include"StarColoring.hpp"

using std::min;
using std::max;

const int N = 50;

Eigen::MatrixX<double> arrowhead(int N, int b = 1){
    Eigen::MatrixX<double> A = Eigen::MatrixX<double>::Zero(N,N);
    for(int i=0; i<N; i++){
        for(int j=1; j<=b; j++){
            A(i,N-j) = 1;
            A(N-j,i) = 1;
        }
        //A(i,i) = 2;
    }
    return A;
}

Eigen::MatrixX<double> band(int N, int bw = 1){
    Eigen::MatrixX<double> A = Eigen::MatrixX<double>::Zero(N,N);
    for(int i=0; i<N; i++){
        for(int j=max(0,i-bw); j<=min(N-1,i+bw); j++){
            A(i,j) = i==j ? 2 : 1;
        }
    }
    return A;
}

int main(){
    /*Eigen::MatrixX<double> A = Eigen::MatrixX<double>::Random(N,N);
    A = Eigen::MatrixX<double>(
        (A.array() > 0.5).cast<double>()).triangularView<Eigen::Upper>();
    A = A + A.transpose().eval();
    A = Eigen::MatrixX<double>((A.array() != 0).cast<double>());*/
    //Eigen::MatrixX<double> A = arrowhead(N,2);
    Eigen::MatrixX<double> A = band(N,4);
    Eigen::SparseMatrix<double> S = A.sparseView();
    
    StarColoring G(S);
    G.distanceTwoColor();
    G.starColor();

    //std::cout << A << std::endl << std::endl;
    std::cout << G.getColumnSeedMatrix() << std::endl << std::endl;
    
    std::cout << G.getColumnSeedMatrix()*A << std::endl;

    //G.writeGraph();
}