#include <cmath>
#include <algorithm>
#include <functional>
using namespace std;
#include "jsp.hpp"

template <typename set_t>
inline jsp<set_t>::jsp(const double &x) : v(x)
{
  nz.clear();
};
template <typename set_t>
inline jsp<set_t>::jsp(const double &x, const set_t &nz) : v(x), nz(nz){};
template <typename set_t>
inline jsp<set_t>::jsp() : v(0) { nz.clear(); };

template <typename set_t>
ostream &operator<<(ostream &os, const jsp<set_t> &x)
{
  os << x.v;
  return os;
}

template <typename set_t>
inline jsp<set_t> &jsp<set_t>::operator=(const jsp<set_t> &x)
{
  if (this == &x)
    return *this;
  v = x.v;
  nz = x.nz;
  return *this;
}

template <typename set_t>
inline jsp<set_t> &jsp<set_t>::operator=(const double &a)
{
  this->v = a;
  nz.clear();
  return *this;
}

template <typename set_t>
inline jsp<set_t> &jsp<set_t>::operator+=(const jsp<set_t> &x)
{
  if (this == &x)
    return *this;
  v += x.v;
  nz.insert(x.nz.begin(), x.nz.end());
  return *this;
}

template <typename set_t>
inline jsp<set_t> &jsp<set_t>::operator+=(const double &x)
{
  v += x;
  return *this;
}

template <typename set_t>
inline jsp<set_t> operator*(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp(x1.v * x2.v);
  if (x2.v || !x2.nz.empty())
    tmp.nz.insert(x1.nz.begin(), x1.nz.end());
  if (x1.v || !x1.nz.empty())
    tmp.nz.insert(x2.nz.begin(), x2.nz.end());
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator*(const double &a1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp;
  tmp.v = a1 * x2.v;
  tmp.nz = x2.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator*(const jsp<set_t> &x1, const double &a2)
{
  jsp<set_t> tmp;
  tmp.v = x1.v * a2;
  tmp.nz = x1.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator+(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp;
  tmp.v = x1.v + x2.v;
  tmp.nz = x1.nz;
  tmp.nz.insert(x2.nz.begin(), x2.nz.end());
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator+(const double &a1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp;
  tmp.v = a1 + x2.v;
  tmp.nz = x2.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator+(const jsp<set_t> &x1, const double &a2)
{
  jsp<set_t> tmp;
  tmp.v = x1.v + a2;
  tmp.nz = x1.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator-(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp;
  tmp.v = x1.v - x2.v;
  tmp.nz = x1.nz;
  tmp.nz.insert(x2.nz.begin(), x2.nz.end());
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator-(const double &a1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp;
  tmp.v = a1 - x2.v;
  tmp.nz = x2.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator-(const jsp<set_t> &x1, const double &a2)
{
  jsp<set_t> tmp;
  tmp.v = x1.v - a2;
  tmp.nz = x1.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator-(const jsp<set_t> &x)
{
  jsp<set_t> tmp;
  tmp.v = -x.v;
  tmp.nz = x.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator/(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp;
  tmp.v = x1.v / x2.v;
  tmp.nz = x1.nz;
  tmp.nz.insert(x2.nz.begin(), x2.nz.end());
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator/(const double &a1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp;
  tmp.v = a1 / x2.v;
  tmp.nz = x2.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> operator/(const jsp<set_t> &x1, const double &a2)
{
  jsp<set_t> tmp;
  tmp.v = x1.v / a2;
  tmp.nz = x1.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> pow(const jsp<set_t> &x, const double &c)
{
  jsp<set_t> tmp;
  tmp.v = pow(x.v, c);
  tmp.nz = x.nz;
  return tmp;
}

template <typename set_t>
inline jsp<set_t> pow(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp;
  tmp.v = pow(x1.v, x2.v);
  tmp.nz = x1.nz;
  tmp.nz.insert(x2.nz.begin(), x2.nz.end());
  return tmp;
}

template <typename set_t>
inline jsp<set_t> pow(const double &a1, const jsp<set_t> &x2)
{
  jsp<set_t> tmp;
  tmp.v = pow(a1, x2.v);
  tmp.nz = x2.nz;
  return tmp;
}

template <typename set_t>
inline bool operator<(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  return (x1.v < x2.v);
}

template <typename set_t>
inline bool operator<(const double &a1, const jsp<set_t> &x2)
{
  return (a1 < x2.v);
}

template <typename set_t>
inline bool operator<(const jsp<set_t> &x1, const double &a2)
{
  return (x1.v < a2);
}

template <typename set_t>
inline bool operator<=(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  return (x1.v <= x2.v);
}

template <typename set_t>
inline bool operator<=(const double &a1, const jsp<set_t> &x2)
{
  return (a1 <= x2.v);
}

template <typename set_t>
inline bool operator<=(const jsp<set_t> &x1, const double &a2)
{
  return (x1.v <= a2);
}

template <typename set_t>
inline bool operator>(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  return (x1.v > x2.v);
}

template <typename set_t>
inline bool operator>(const double &a1, const jsp<set_t> &x2)
{
  return (a1 > x2.v);
}

template <typename set_t>
inline bool operator>(const jsp<set_t> &x1, const double &a2)
{
  return (x1.v > a2);
}

template <typename set_t>
inline bool operator>=(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  return (x1.v >= x2.v);
}

template <typename set_t>
inline bool operator>=(const double &a1, const jsp<set_t> &x2)
{
  return (a1 >= x2.v);
}

template <typename set_t>
inline bool operator>=(const jsp<set_t> &x1, const double &a2)
{
  return (x1.v >= a2);
}

template <typename set_t>
inline bool operator==(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  return (x1.v == x2.v);
}

template <typename set_t>
inline bool operator==(const double &a1, const jsp<set_t> &x2)
{
  return (a1 == x2.v);
}

template <typename set_t>
inline bool operator==(const jsp<set_t> &x1, const double &a2)
{
  return (x1.v == a2);
}

template <typename set_t>
inline bool operator!=(const jsp<set_t> &x1, const jsp<set_t> &x2)
{
  return (x1.v != x2.v);
}

template <typename set_t>
inline bool operator!=(const double &a1, const jsp<set_t> &x2)
{
  return (a1 != x2.v);
}

template <typename set_t>
inline bool operator!=(const jsp<set_t> &x1, const double &a2)
{
  return (x1.v != a2);
}

#define JSP_UNARY_OP(OP)                    \
  template <typename set_t>                 \
  inline jsp<set_t> OP(const jsp<set_t> &x) \
  {                                         \
    return jsp<set_t>(OP(x.v), x.nz);       \
  }

JSP_UNARY_OP(sin)
JSP_UNARY_OP(cos)
JSP_UNARY_OP(exp)
JSP_UNARY_OP(atan)
JSP_UNARY_OP(sqrt)
JSP_UNARY_OP(log)
JSP_UNARY_OP(log10)
#undef JSP_UNARY_OP

template <typename set_t>
inline int ceil(const jsp<set_t> &x)
{
  return ceil(x.v);
}