#ifndef JSP_INCLUDED_
#define JSP_INCLUDED_
//#define FLAT
#include <iostream>
#include <set>

using namespace std;

template <typename set_t = std::set<int>>
struct jsp
{
  double v;
  set_t nz;
  inline jsp(const double &x);
  inline jsp(const double &x, const set_t &nz);
  inline jsp();
  inline jsp &operator=(const jsp &x);
  inline jsp &operator=(const double &a);

  inline jsp &operator+=(const jsp &x);
  inline jsp &operator+=(const double &a);

  template <typename T>
  friend ostream &operator<<(ostream &os, const jsp<T> &x);
};

template <typename set_t>
inline jsp<set_t> operator*(const jsp<set_t> &x1, const jsp<set_t> &x2);

template <typename set_t>
inline jsp<set_t> operator*(const double &a1, const jsp<set_t> &x2);

template <typename set_t>
inline jsp<set_t> operator*(const jsp<set_t> &x1, const double &a2);

template <typename set_t>
inline jsp<set_t> operator+(const jsp<set_t> &x1, const jsp<set_t> &x2);

template <typename set_t>
inline jsp<set_t> operator+(const double &a1, const jsp<set_t> &x2);

template <typename set_t>
inline jsp<set_t> operator+(const jsp<set_t> &x1, const double &a2);

template <typename set_t>
inline jsp<set_t> operator-(const jsp<set_t> &x1, const jsp<set_t> &x2);
template <typename set_t>
inline jsp<set_t> operator-(const double &a1, const jsp<set_t> &x2);
template <typename set_t>
inline jsp<set_t> operator-(const jsp<set_t> &x1, const double &a2);
template <typename set_t>
inline jsp<set_t> operator-(const jsp<set_t> &x);

template <typename set_t>
inline jsp<set_t> operator/(const jsp<set_t> &x1, const jsp<set_t> &x2);
template <typename set_t>
inline jsp<set_t> operator/(const double &a1, const jsp<set_t> &x2);
template <typename set_t>
inline jsp<set_t> operator/(const jsp<set_t> &x1, const double &a2);

template <typename set_t>
inline bool operator<(const jsp<set_t> &x1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator<(const double &a1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator<(const jsp<set_t> &x1, const double &a2);

template <typename set_t>
inline bool operator<=(const jsp<set_t> &x1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator<=(const double &a1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator<=(const jsp<set_t> &x1, const double &a2);

template <typename set_t>
inline bool operator>(const jsp<set_t> &x1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator>(const double &a1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator>(const jsp<set_t> &x1, const double &a2);

template <typename set_t>
inline bool operator>=(const jsp<set_t> &x1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator>=(const double &a1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator>=(const jsp<set_t> &x1, const double &a2);

template <typename set_t>
inline bool operator==(const jsp<set_t> &x1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator==(const double &a1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator==(const jsp<set_t> &x1, const double &a2);

template <typename set_t>
inline bool operator!=(const jsp<set_t> &x1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator!=(const double &a1, const jsp<set_t> &x2);
template <typename set_t>
inline bool operator!=(const jsp<set_t> &x1, const double &a2);

template <typename set_t>
inline jsp<set_t> sin(const jsp<set_t> &x);
template <typename set_t>
inline jsp<set_t> cos(const jsp<set_t> &x);
template <typename set_t>
inline jsp<set_t> exp(const jsp<set_t> &x);
template <typename set_t>
inline jsp<set_t> atan(const jsp<set_t> &x);
template <typename set_t>
inline jsp<set_t> sqrt(const jsp<set_t> &x);
template <typename set_t>
inline jsp<set_t> log(const jsp<set_t> &x);
template <typename set_t>
inline jsp<set_t> log10(const jsp<set_t> &x);
template <typename set_t>
inline int ceil(const jsp<set_t> &x);

template <typename set_t>
inline jsp<set_t> pow(const jsp<set_t> &x, const double &c);
template <typename set_t>
inline jsp<set_t> pow(const jsp<set_t> &x1, const jsp<set_t> &x2);
template <typename set_t>
inline jsp<set_t> pow(const double &a1, const jsp<set_t> &x2);

#include "jsp_impl.hpp"

#endif
