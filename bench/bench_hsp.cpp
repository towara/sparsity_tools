#include <benchmark/benchmark.h>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Sparse>
#include <iostream>

#include <set>
#include <unordered_set>

// https://github.com/slavenf/sfl-library
#include "../sfl-library/include/sfl/small_flat_set.hpp"
#include "../sfl-library/include/sfl/small_unordered_flat_set.hpp"
#include "../flat_datastructures/flat_set.hpp"
#include "../hsp/hsp.hpp"

template <typename T>
T f(Eigen::SparseMatrix<T> &A, Eigen::VectorX<T> &x)
{
    T sum = 0.0;

    for (int k = 0; k < A.outerSize(); ++k)
    {
        for (typename Eigen::SparseMatrix<T>::InnerIterator it(A, k); it; ++it)
        {
            if (it.row() >= it.col())
            {
                sum += x[it.row()] * x[it.col()];
            }
        }
    }

    return sum;
}

// D is approx. sparsity of A in percent
template <typename fod_t, typename sod_t, int D>
static void BM_SET(benchmark::State &state)
{
    using hsp_t = hsp<fod_t, sod_t>;
    const int N = state.range(0);

    Eigen::MatrixX<hsp_t> A = Eigen::MatrixX<hsp_t>::Random(N, N);
    A = (A.array() > (1.0 - D / 100.0)).template cast<hsp_t>();

    Eigen::SparseMatrix<hsp_t> B = A.sparseView();

    Eigen::VectorX<hsp_t> x = Eigen::VectorX<hsp_t>::Ones(A.cols());
    for (int i = 0; i < x.size(); i++)
    {
        x[i].fod.insert(i);
    }

    hsp_t y;
    for (auto _ : state)
    {
        benchmark::DoNotOptimize(y = f(B, x));
    }
}

struct pair_hash
{
    template <class T1, class T2>
    std::size_t operator () (std::pair<T1, T2> const &pair) const
    {
        std::size_t h1 = std::hash<T1>()(pair.first);
        std::size_t h2 = std::hash<T2>()(pair.second);
 
        return h1 ^ h2;
    }
};

const int lower = 128;
const int upper = 128;

BENCHMARK_TEMPLATE(BM_SET, std::set<int>, std::set<pair<int,int>>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::set<int>, std::set<pair<int,int>>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::set<int>, std::set<pair<int,int>>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::set<int>, std::set<pair<int,int>>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, std::unordered_set<int>, std::unordered_set<pair<int,int>,pair_hash>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::unordered_set<int>, std::unordered_set<pair<int,int>,pair_hash>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::unordered_set<int>, std::unordered_set<pair<int,int>,pair_hash>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::unordered_set<int>, std::unordered_set<pair<int,int>,pair_hash>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,0>, sfl::small_flat_set<pair<int,int>,0>,   0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,0>, sfl::small_flat_set<pair<int,int>,0>,  10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,0>, sfl::small_flat_set<pair<int,int>,0>,  20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,0>, sfl::small_flat_set<pair<int,int>,0>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,4>, sfl::small_flat_set<pair<int,int>,4>,   0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,4>, sfl::small_flat_set<pair<int,int>,4>,  10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,4>, sfl::small_flat_set<pair<int,int>,4>,  20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,4>, sfl::small_flat_set<pair<int,int>,4>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,4>, sfl::small_flat_set<pair<int,int>,16>,   0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,16>, sfl::small_flat_set<pair<int,int>,16>,  10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,16>, sfl::small_flat_set<pair<int,int>,16>,  20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int,16>, sfl::small_flat_set<pair<int,int>,16>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, flat_set<int>, flat_set<pair<int,int>>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, flat_set<int>, flat_set<pair<int,int>>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, flat_set<int>, flat_set<pair<int,int>>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, flat_set<int>, flat_set<pair<int,int>>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_MAIN();