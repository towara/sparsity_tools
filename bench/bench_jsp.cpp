#include <benchmark/benchmark.h>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Sparse>
#include <iostream>

#include <set>
#include <unordered_set>

// https://github.com/slavenf/sfl-library
#include "../sfl-library/include/sfl/small_flat_set.hpp"
#include "../sfl-library/include/sfl/small_unordered_flat_set.hpp"
#include "../flat_datastructures/flat_set.hpp"
#include "../jsp/jsp.hpp"

template <typename MAT, typename T>
Eigen::VectorX<T> f(MAT &A, Eigen::VectorX<T> &x)
{
    return A * x;
}

// D is approx. sparsity of A in percent (e.g. 20% non-zeros)
template <typename set_t, int D>
static void BM_SET(benchmark::State &state)
{
    using jsp_t = jsp<set_t>;
    const int N = state.range(0);

    Eigen::MatrixX<jsp_t> A = Eigen::MatrixX<jsp_t>::Random(N, N);
    A = (A.array() > (1.0 - D / 100.0)).template cast<jsp_t>();

    Eigen::SparseMatrix<jsp_t> B = A.sparseView();

    Eigen::VectorX<jsp_t> x = Eigen::VectorX<jsp_t>::Ones(A.cols());
    // seed the identifiers for x
    for (int i = 0; i < x.size(); i++)
    {
        x[i].nz.insert(i);
    }

    Eigen::VectorX<jsp_t> y;
    for (auto _ : state)
    {
        benchmark::DoNotOptimize(y = f(B, x));
    }
}

const int lower = 512;
const int upper = 512;

BENCHMARK_TEMPLATE(BM_SET, std::set<int>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::set<int>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::set<int>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::set<int>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, flat_set<int>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, flat_set<int>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, flat_set<int>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, flat_set<int>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 0>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 0>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 0>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 0>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 4>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 4>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 4>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 4>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 16>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 16>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 16>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_flat_set<int, 16>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, std::unordered_set<int>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::unordered_set<int>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::unordered_set<int>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, std::unordered_set<int>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_TEMPLATE(BM_SET, sfl::small_unordered_flat_set<int, 16>, 0)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_unordered_flat_set<int, 16>, 10)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_unordered_flat_set<int, 16>, 20)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(BM_SET, sfl::small_unordered_flat_set<int, 16>, 100)->RangeMultiplier(2)->Range(lower, upper)->Unit(benchmark::kMillisecond);

BENCHMARK_MAIN();
